
Feature: Validate Open Weather App
	Background: Set up base url
	Given Base URI "http://api.openweathermap.org/data/2.5/"
	
  @regression  @positive
  Scenario: Validate Country Code and City ID
  	Given Get the current weather for "London" city of "UK" country
  	Then Validate that country code is "GB"
  	And  City id is 2643743
  
   @regression @positive
    Scenario: Validate city with multiple countries
  	Given The city name is "Bombay"
  	Then It has more than 1 countries
  	Then Validate each country is different
  	
  @regression @positive
    Scenario: Validate city with multiple countries
  	Given The city name is "London"
  	Then It has more than 1 countries
  	Then Validate atleast 3 country is different

  	  
  @regression @negative
    Scenario: Validate city with multiple countries
  	Given The city name is "London"
  	Then It has more than 1 countries
  	Then Validate each country is not different