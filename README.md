Feature: Validate Open Weather App via APi to test get weather of city and city with non unique countries.

TC01:	Scenario: Validate Country Code and City ID
Get current weather for a city and country code (as parameter) and validate that country and id is correct (API - https://openweathermap.org/current)

TC02:	Scenario: Validate city with multiple countries
Test where city name is not unique and exists in multiple countries 

TC03:
Negative test like city has multiple countries.