package com.demo.weather;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.restassured.RestAssured;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;

public class StepDefinitions {
	RequestSpecification httpRequest;
	Response lastResponse;

	@Given("^Base URI \"([^\"]+)\"$")
	public void set_base_uri(String baseUrl) {
		RestAssured.baseURI = baseUrl;
		this.httpRequest = RestAssured.given();
	}

	@Given("^Get the current weather for \"([^\"]+)\" city of \"([^\"]+)\" country$")
	public void get_the_current_weather(String city, String country) {
		this.lastResponse = this.httpRequest.queryParam("q", city + "," + country)
				.queryParam("appid", "1cb6ace31e50401f28b864f0b23fdc68").get("/weather");
		assertTrue(this.lastResponse.getStatusCode() == 200);
	}

	@Then("^Validate that country code is \"([^\"]+)\"$")
	public void validate_that_country_code(String expectedCountryCode) {
		String actualCountryCode = this.lastResponse.getBody().jsonPath().getString("sys.country");
		assertTrue(actualCountryCode.trim().equalsIgnoreCase(expectedCountryCode.trim()));
	}

	@Then("City id is {int}")
	public void validate_city_id_is(Integer expectedCityId) {
		int actualCityId = this.lastResponse.getBody().jsonPath().getInt("id");
		assertTrue(actualCityId == expectedCityId);
	}
	@Given("The city name is \"([^\"]+)\"$")
	public void the_city_name_is(String cityName) {
		this.lastResponse = this.httpRequest.queryParam("q", cityName)
				.queryParam("appid", "1cb6ace31e50401f28b864f0b23fdc68").get("/find");
		assertTrue(this.lastResponse.getStatusCode() == 200);
	}

	@Then("It has more than {int} countries")
	public void it_has_more_than_countries(Integer size) {
		List<String> list = this.lastResponse.getBody().jsonPath().getList("list.sys.country");
		assertNotNull(list);
		assertTrue(list.size() > size);
	}

	@Then("Validate each country is different")
	public void validate_each_country_is_different() {
		List<String> list = this.lastResponse.getBody().jsonPath().getList("list.sys.country");
		Set<String> listOfUniqueCountries = new HashSet<String>();
		for(int i = 0; i < list.size(); i++) {
			listOfUniqueCountries.add(list.get(i));
		}
		assertTrue(listOfUniqueCountries.size() == list.size());
	}
	@Then("Validate each country is not different")
	public void validate_hasduplicate() {
		List<String> list = this.lastResponse.getBody().jsonPath().getList("list.sys.country");
		Set<String> listOfUniqueCountries = new HashSet<String>();
		for(int i = 0; i < list.size(); i++) {
			listOfUniqueCountries.add(list.get(i));
		}
		assertTrue(listOfUniqueCountries.size() != list.size());
	}
	
	@Then("Validate atleast {int} country is different")
	public void validate_country_is_different(Integer size) {
		List<String> list = this.lastResponse.getBody().jsonPath().getList("list.sys.country");
		Set<String> listOfUniqueCountries = new HashSet<String>();
		for(int i = 0; i < list.size(); i++) {
			listOfUniqueCountries.add(list.get(i));
		}
		assertTrue(listOfUniqueCountries.size() >=size );
	}
}
